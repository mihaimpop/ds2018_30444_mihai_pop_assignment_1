package dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class CitiesDAO {

  private SessionFactory sessionFactory;

  public CitiesDAO(SessionFactory factory) {
    this.sessionFactory = factory;
  }

  public List getAllCities() {
    Session session = sessionFactory.openSession();
    Transaction tx = null;
    List cities = null;

    try {
      tx = session.getTransaction();
      tx.begin();
      Query query = session.createQuery("FROM Cities");
      cities = query.list();
      tx.commit();
    } catch (Exception e) {
      if (tx != null) {
        tx.rollback();
      }
      e.printStackTrace();
    } finally {
      session.close();
    }

    return cities;
  }
}
