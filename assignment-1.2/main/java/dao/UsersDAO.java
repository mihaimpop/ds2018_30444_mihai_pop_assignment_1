package dao;

import model.Users;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class UsersDAO {

  private SessionFactory sessionFactory;

  public UsersDAO(SessionFactory factory) {
    this.sessionFactory = factory;
  }

  public Users getUser(String username) {
    Session session = sessionFactory.openSession();
    Transaction tx = null;
    Users user = null;

    try {
      tx = session.getTransaction();
      tx.begin();
      Query query = session.createQuery("FROM Users WHERE username='" + username + "'");
      user = (Users) query.uniqueResult();
      tx.commit();
    } catch (Exception e) {
      if (tx != null) {
        tx.rollback();
      }
      e.printStackTrace();
    } finally {
      session.close();
    }

    return user;
  }

  public List getAllClients() {
    Session session = sessionFactory.openSession();
    Transaction tx = null;
    List clients = null;

    try {
      tx = session.getTransaction();
      tx.begin();
      Query query = session.createQuery("FROM Users WHERE role = 'USER'");
      clients = query.list();
      tx.commit();
    } catch (Exception e) {
      if (tx != null) {
        tx.rollback();
      }
      e.printStackTrace();
    } finally {
      session.close();
    }

    return clients;
  }
}
