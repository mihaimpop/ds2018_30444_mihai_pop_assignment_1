package model;

public class Cities implements java.io.Serializable {
  private Integer id;
  private String cityName;
  private String latitude;
  private String longitude;

  public Cities() {
  }

  public Cities(Integer id, String cityName, String latitude, String longitude) {
    this.id = id;
    this.cityName = cityName;
    this.latitude = latitude;
    this.longitude = longitude;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getCityName() {
    return cityName;
  }

  public void setCityName(String cityName) {
    this.cityName = cityName;
  }

  public String getLatitude() {
    return latitude;
  }

  public void setLatitude(String latitude) {
    this.latitude = latitude;
  }

  public String getLongitude() {
    return longitude;
  }

  public void setLongitude(String longitude) {
    this.longitude = longitude;
  }
}
