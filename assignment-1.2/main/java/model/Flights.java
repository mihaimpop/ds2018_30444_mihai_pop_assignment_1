package model;

import java.sql.Time;
import java.util.Date;

public class Flights implements java.io.Serializable {
  private Integer flightNumber;
  private String clientUsername;
  private String airplaneType;
  private String departureCity;
  private String departureLatitude;
  private String departureLongitude;
  private Date departureDate;
  private Time departureHour;
  private String arrivalCity;
  private String arrivalLatitude;
  private String arrivalLongitude;
  private Date arrivalDate;
  private Time arrivalHour;

  public Flights() {

  }

  public Flights(Integer flightNumber, String clientUsername, String airplaneType, String departureCity, String departureLatitude, String departureLongitude, Date departureDate, Time departureHour, String arrivalCity, String arrivalLatitude, String arrivalLongitude, Date arrivalDate, Time arrivalHour) {
    this.flightNumber = flightNumber;
    this.clientUsername = clientUsername;
    this.airplaneType = airplaneType;
    this.departureCity = departureCity;
    this.departureLatitude = departureLatitude;
    this.departureLongitude = departureLongitude;
    this.departureDate = departureDate;
    this.departureHour = departureHour;
    this.arrivalCity = arrivalCity;
    this.arrivalLatitude = arrivalLatitude;
    this.arrivalLongitude = arrivalLongitude;
    this.arrivalDate = arrivalDate;
    this.arrivalHour = arrivalHour;
  }

  public Integer getFlightNumber() {
    return flightNumber;
  }

  public void setFlightNumber(Integer flightNumber) {
    this.flightNumber = flightNumber;
  }

  public String getClientUsername() {
    return clientUsername;
  }

  public void setClientUsername(String clientUsername) {
    this.clientUsername = clientUsername;
  }

  public String getAirplaneType() {
    return airplaneType;
  }

  public void setAirplaneType(String airplaneType) {
    this.airplaneType = airplaneType;
  }

  public String getDepartureCity() {
    return departureCity;
  }

  public void setDepartureCity(String departureCity) {
    this.departureCity = departureCity;
  }

  public Date getDepartureDate() {
    return departureDate;
  }

  public void setDepartureDate(Date departureDate) {
    this.departureDate = departureDate;
  }

  public Time getDepartureHour() {
    return departureHour;
  }

  public void setDepartureHour(Time departureHour) {
    this.departureHour = departureHour;
  }

  public String getArrivalCity() {
    return arrivalCity;
  }

  public void setArrivalCity(String arrivalCity) {
    this.arrivalCity = arrivalCity;
  }

  public Date getArrivalDate() {
    return arrivalDate;
  }

  public void setArrivalDate(Date arrivalDate) {
    this.arrivalDate = arrivalDate;
  }

  public Time getArrivalHour() {
    return arrivalHour;
  }

  public void setArrivalHour(Time arrivalHour) {
    this.arrivalHour = arrivalHour;
  }

  public String getDepartureLatitude() {
    return departureLatitude;
  }

  public void setDepartureLatitude(String departureLatitude) {
    this.departureLatitude = departureLatitude;
  }

  public String getDepartureLongitude() {
    return departureLongitude;
  }

  public void setDepartureLongitude(String departureLongitude) {
    this.departureLongitude = departureLongitude;
  }

  public String getArrivalLatitude() {
    return arrivalLatitude;
  }

  public void setArrivalLatitude(String arrivalLatitude) {
    this.arrivalLatitude = arrivalLatitude;
  }

  public String getArrivalLongitude() {
    return arrivalLongitude;
  }

  public void setArrivalLongitude(String arrivalLongitude) {
    this.arrivalLongitude = arrivalLongitude;
  }
}
