package servlets;

import dao.FlightsDAO;
import model.Flights;
import org.hibernate.cfg.Configuration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@WebServlet(name = "AdminServlet", urlPatterns = {"/AdminServlet"})
public class AdminServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String formType = request.getParameter("form_type");

    if ("insert".equals(formType)) {
      // get form content
      DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

      Integer flightNumber = Integer.parseInt(request.getParameter("flight_number"));
      String client = request.getParameter("client");
      String airplaneType = request.getParameter("airplane_type");

      String departureCityRaw = request.getParameter("departure_city");
      String departureTokens[] = departureCityRaw.split(",");
      String departureCity = departureTokens[0];
      String departureLat = departureTokens[1];
      String departureLong = departureTokens[2];

      String arrivalCityRaw = request.getParameter("arrival_city");
      String arrivalTokens[] = arrivalCityRaw.split(",");
      String arrivalCity = arrivalTokens[0];
      String arrivalLat = arrivalTokens[1];
      String arrivalLong = arrivalTokens[2];

      Date departureDate = null;
      Time departureTime = null;
      Date arrivalDate = null;
      Time arrivalTime = null;
      String timeTokens[];

      try {
        String departureDateString = request.getParameter("departure_date");
        departureDate = dateFormat.parse(departureDateString);

        String departureTimeString = request.getParameter("departure_time");
        timeTokens = departureTimeString.split(":");
        departureTime = new Time(Integer.parseInt(timeTokens[0]), Integer.parseInt(timeTokens[1]), 0);

        String arrivalDateString = request.getParameter("arrival_date");
        arrivalDate = dateFormat.parse(arrivalDateString);

        String arrivalTimeString = request.getParameter("arrival_time");
        timeTokens = arrivalTimeString.split(":");
        arrivalTime = new Time(Integer.parseInt(timeTokens[0]), Integer.parseInt(timeTokens[1]), 0);
      } catch (ParseException e) {
        e.printStackTrace();
      }

      Flights newFlight = new Flights(flightNumber, client, airplaneType,
          departureCity, departureLat, departureLong, departureDate, departureTime,
          arrivalCity, arrivalLat, arrivalLong, arrivalDate, arrivalTime);

      HttpSession session = request.getSession(false);

      FlightsDAO flightsDAO = new FlightsDAO(new Configuration().configure().buildSessionFactory());
      newFlight = flightsDAO.addFlight(newFlight);

      if (newFlight.getFlightNumber() == null) {
        // something went wrong
        session.setAttribute("form_message", "Flight was not inserted!.");
        response.sendRedirect("admin-page.jsp");
      } else {
        session.setAttribute("form_message", "Flight nr. " + newFlight.getFlightNumber() + " was inserted");

        List allFlights = flightsDAO.getAllFlights();
        session.setAttribute("allFlights", allFlights);

        response.sendRedirect("admin-page.jsp");
      }

    } else if ("delete".equals(formType)) {

      String delFlightNumber = request.getParameter("del_flight_number");
      HttpSession session = request.getSession(false);

      FlightsDAO flightsDAO = new FlightsDAO(new Configuration().configure().buildSessionFactory());
      flightsDAO.deleteFlight(Integer.parseInt(delFlightNumber));

      List allFlights = flightsDAO.getAllFlights();
      session.setAttribute("allFlights", allFlights);

      response.sendRedirect("admin-page.jsp");
    } else if ("update".equals(formType)) {

      // get form content
      DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

      Integer flightNumber = Integer.parseInt(request.getParameter("flight_number"));
      String client = request.getParameter("client");
      String airplaneType = request.getParameter("airplane_type");

      String departureCityRaw = request.getParameter("departure_city");
      String departureTokens[] = departureCityRaw.split(",");
      String departureCity = departureTokens[0];
      String departureLat = departureTokens[1];
      String departureLong = departureTokens[2];

      String arrivalCityRaw = request.getParameter("arrival_city");
      String arrivalTokens[] = arrivalCityRaw.split(",");
      String arrivalCity = arrivalTokens[0];
      String arrivalLat = arrivalTokens[1];
      String arrivalLong = arrivalTokens[2];

      Date departureDate = null;
      Time departureTime = null;
      Date arrivalDate = null;
      Time arrivalTime = null;
      String timeTokens[];

      try {
        String departureDateString = request.getParameter("departure_date");
        departureDate = dateFormat.parse(departureDateString);

        String departureTimeString = request.getParameter("departure_time");
        timeTokens = departureTimeString.split(":");
        departureTime = new Time(Integer.parseInt(timeTokens[0]), Integer.parseInt(timeTokens[1]), 0);

        String arrivalDateString = request.getParameter("arrival_date");
        arrivalDate = dateFormat.parse(arrivalDateString);

        String arrivalTimeString = request.getParameter("arrival_time");
        timeTokens = arrivalTimeString.split(":");
        arrivalTime = new Time(Integer.parseInt(timeTokens[0]), Integer.parseInt(timeTokens[1]), 0);
      } catch (ParseException e) {
        e.printStackTrace();
      }

      Flights flight = new Flights(flightNumber, client, airplaneType,
          departureCity, departureLat, departureLong, departureDate, departureTime,
          arrivalCity, arrivalLat, arrivalLong, arrivalDate, arrivalTime);
      flight.setFlightNumber(flightNumber);

      HttpSession session = request.getSession(false);

      FlightsDAO flightsDAO = new FlightsDAO(new Configuration().configure().buildSessionFactory());
      flightsDAO.updateFlight(flight);

      List allFlights = flightsDAO.getAllFlights();
      session.setAttribute("allFlights", allFlights);

      response.sendRedirect("admin-page.jsp");
    }

  }

  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    doGet(request, response);
  }

}
