<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ page import="model.Users" %>

<%@ page import="java.util.List" %>
<%@ page import="model.Flights" %>
<% Users user = (Users) session.getAttribute("user");
  if (user == null) {
    System.out.println("User is null...");
    response.sendRedirect("user-page.jsp");
    return;
  } else if (user.getRole().equals("ADMIN")) {
    response.sendRedirect("admin-page.jsp");
    return;
  }

  List<Flights> clientFlights = (List) session.getAttribute("clientFlights");
%>
<!DOCTYPE html>
<html>
<head>
  <script src="client.js"></script>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <link rel="stylesheet" type="text/css" href="user-page.css">
  <title>User page</title>
</head>
<body>
<a href="login.jsp" id="logout" class="logout">LOGOUT</a>
<p class="welcome">Welcome to your account, <b><%=user.getUsername() %>
</b></p>
<h1 class="user-page-title">User page</h1>
<p class="flights-info">You have <%=clientFlights.size() %> flights:</p>
<table class="flights-table" id="flights-table">
  <tr>
    <th>Flight number</th>
    <th>Airplane type</th>
    <th>Departure city</th>
    <th>Departure date</th>
    <th>Departure time</th>
    <th>Arrival city</th>
    <th>Arrival date</th>
    <th>Arrival time</th>
      <% for (Flights flight : clientFlights) { %>
  <tr>
    <td><%=flight.getFlightNumber() %>
    </td>
    <td><%=flight.getAirplaneType() %>
    </td>
    <td><%=flight.getDepartureCity() %><br>
      <a href=""
         onclick="displayTime(<%=flight.getDepartureLatitude() %>,<%=flight.getDepartureLongitude() %>); return false;">
        Local time
      </a></td>
    <td><%=flight.getDepartureDate() %>
    </td>
    <td><%=flight.getDepartureHour() %>
    </td>
    <td><%=flight.getArrivalCity() %><br>
      <a href=""
         onclick="displayTime(<%=flight.getArrivalLatitude() %>,<%=flight.getArrivalLongitude() %>); return false;">
        Local time
      </a>
    </td>
    <td><%=flight.getArrivalDate() %>
    </td>
    <td><%=flight.getArrivalHour() %>
    </td>
  </tr>
  <% } %>
</table>
</body>
</html>