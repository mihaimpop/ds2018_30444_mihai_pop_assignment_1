<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ page import="model.Users" %>
<%@ page import="model.Flights" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %>
<%@ page import="model.Cities" %>
<% Users user = (Users) session.getAttribute("user");
  if (user == null) {
    System.out.println("User is null...");
    response.sendRedirect("login.jsp");
    return;
  }

  List<Flights> allFlights = (List) session.getAttribute("allFlights");
  List<Users> allClients = (List) session.getAttribute("allClients");
  List<Cities> allCities = (List) session.getAttribute("allCities");

  String formMessage = (String) session.getAttribute("form_message");
  session.removeAttribute("form_message");
%>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <link rel="stylesheet" type="text/css" href="admin-page.css">
  <title>Admin page</title>
</head>
<body>
<% if (user.getRole().equals("ADMIN")) { %>
<div class="upper-area">
  <a id="logout" href="login.jsp" class="logout">LOGOUT</a>
  <p class="welcome">Welcome to your account, <b><%=user.getUsername() %>
  </b></p>
  <h1 class="admin-page-title">Admin page</h1>

  <h4 class="all-flights">All flights:</h4>
  <table id="flights-table" class="flights-table">
    <tr>
      <th style="width: 60px">Client</th>
      <th style="width: 70px">Flight number</th>
      <th style="width: 100px">Airplane type</th>
      <th style="width: 100px">Departure city</th>
      <th style="width: 130px">Departure date</th>
      <th style="width: 90px">Departure time</th>
      <th style="width: 100px">Arrival city</th>
      <th style="width: 130px">Arrival date</th>
      <th style="width: 90px">Arrival time</th>
      <th style="width: 80px">Update</th>
      <th style="width: 80px">Delete</th>
    </tr>
    <% for (Flights flight : allFlights) { %>
    <tr class="flightRow" id="flight_<%=flight.getFlightNumber() %>">
      <form id="update_flight_<%=flight.getFlightNumber() %>" method="post" action="AdminServlet">
        <input type="hidden" name="form_type" value="update"/>
        <td><input type="text" name="client" class="client" value="<%=flight.getClientUsername() %>"/></td>
        <td><input type="text" name="flight_number" class="number" value="<%=flight.getFlightNumber()%>" readonly/></td>
        <td><input type="text" name="airplane_type" class="type + update_<%=flight.getFlightNumber() %>"
                   value="<%=flight.getAirplaneType() %>"/></td>
        <td class="readonly"><%=flight.getDepartureCity() %><input type="hidden" name="departure_city"
                                                                   value="<%=flight.getDepartureCity() %>,<%=flight.getDepartureLatitude() %>,<%=flight.getDepartureLongitude() %>"/>
        </td>
        <td><input type="date" name="departure_date" class="dep-date + update_<%=flight.getFlightNumber() %>"
                   value="<%=flight.getDepartureDate() %>"/></td>
        <td><input type="time" name="departure_time" class="dep-time + update_<%=flight.getFlightNumber() %>"
                   value="<%=flight.getDepartureHour() %>"/></td>
        <td class="readonly"><%=flight.getArrivalCity() %><input type="hidden" name="arrival_city"
                                                                 value="<%=flight.getArrivalCity() %>,<%=flight.getArrivalLatitude() %>,<%=flight.getArrivalLongitude() %>"/>
        </td>
        <td><input type="date" name="arrival_date" class="arr-date + update_<%=flight.getFlightNumber() %>"
                   value="<%=flight.getArrivalDate() %>"/></td>
        <td><input type="time" name="arrival_time" class="arr-time + update_<%=flight.getFlightNumber() %>"
                   value="<%=flight.getArrivalHour() %>"/></td>
        <td><input class="update-button + update_button_<%=flight.getFlightNumber() %>" type="submit" value="Update"/>
        </td>
      </form>
      <td>
        <form id="deleteFlight" method="post" action="AdminServlet">
          <input type="hidden" name="form_type" value="delete"/>
          <input type="hidden" name="del_flight_number" id="delFlightNumber" value="<%=flight.getFlightNumber() %>"/>
          <input type="submit" value="Delete" class="delete-button"/>
        </form>
      </td>
    </tr>
    <% } %>
  </table>
</div>
`
<h4 class="insert-flight">Insert flight:</h4>
<form id="addFlight" class="add-flight-form" method="post" action="AdminServlet">
  <input type="hidden" name="form_type" value="insert"/>

  <div class="block">
    <label for="client">Client</label>
    <select name="client" id="client" required>
      <% for (Users client : allClients) { %>
      <option value="<%=client.getUsername() %>"><%=client.getUsername() %>
      </option>
      <% } %>
    </select>
  </div>

  <div class="block">
    <label for="flight_number">Flight number</label>
    <input type="text" id="flight_number" name="flight_number" required/>
  </div>

  <div class="block">
    <label for="airplane_type">Airplane type</label>
    <input type="text" id="airplane_type" name="airplane_type" required/>
  </div>

  <div class="block">
    <label for="departure_city">Departure City</label>
    <select name="departure_city" id="departure_city" required>
      <% for (Cities city : allCities) { %>
      <option
          value="<%=city.getCityName() %>,<%=city.getLatitude() %>,<%=city.getLongitude()%>"><%=city.getCityName() %>
      </option>
      <% } %>
    </select>
  </div>
  <br>
  <div class="block">
    <label for="arrival_city">Arrival City</label>
    <select name="arrival_city" id="arrival_city" required>
      <% for (Cities city : allCities) { %>
      <option
          value="<%=city.getCityName() %>,<%=city.getLatitude() %>,<%=city.getLongitude()%>"><%=city.getCityName() %>
      </option>
      <% } %>
    </select>
  </div>

  <div class="block">
    <label for="departure_date">Departure date</label>
    <input type="date" id="departure_date" name="departure_date" required/>
  </div>


  <div class="block">
    <label for="arrival_date">Arrival date</label>
    <input type="date" id="arrival_date" name="arrival_date" required/>
  </div>

  <div class="block">
    <label for="departure_time">Departure time</label>
    <input type="time" id="departure_time" name="departure_time" required/>
  </div>

  <div class="block">
    <label for="arrival_time">Arrival time</label>
    <input type="time" id="arrival_time" name="arrival_time" required/>
  </div>

  <input type="submit" value="Insert" class="button">
  <input type="reset" class="button">
</form>

<p class="message">
  <strong>
    <% if (formMessage != null) {
      out.println(formMessage);
    } %>
  </strong>
</p>


<% } else { %>
<p>You do not have access to this page, because you do not have admin rights.<br>
  <a href="login.jsp">Back to login.</a></p>
<% } %>
</body>
</html>