<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%
  String loginError = (String) session.getAttribute("login_error");
  session.removeAttribute("user");
  session.removeAttribute("login_error");
%>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <link rel="stylesheet" type="text/css" href="login.css">
  <title>Airport Flights</title>
</head>
<body>
<div id="content" class="content">
  <h1 class="title">Airport Flights</h1>
  <form id="login-form" method="post" action="LoginServlet">
    <label for="username">Username:</label>
    <input type="text" id="username" name="username" class="username " required/><br><br>
    <label for="password">Password:</label>
    <input type="password" id="password" name="password" class="username" required/>
    <br><br>
    <div id="buttons">
      <input type="submit" value="Login" class="login-button">
    </div>
    <p class="error">
      <% if (loginError != null) {
        out.println(loginError);
      } %>
    </p>
  </form>
</div>
</body>
</html>